/*
 * cRenderer.cpp
 *
 *  Created on: 06.08.2016
 *      Author: mastercad
 */

#include "../include/cRenderer.h"
#include <sstream>
#include <stdexcept> // für exceptions

#define SPRITE_SIZE   32

bool cRenderer::update() {

  std::vector<scInstanz*>::iterator iter = this->_oInstanzCollection.begin();
  std::vector<scInstanz*>::iterator end = this->_oInstanzCollection.end();

  SDL_RenderClear(this->_oRenderer);
  while(iter != end)
  {
    SDL_RenderCopy(this->_oRenderer, (*iter)->oTexture, (*iter)->rcSrc, (*iter)->rcDest);
      ++iter;
  }
  SDL_RenderPresent(this->_oRenderer);
}

bool cRenderer::initImage(const char* sImagePath, SDL_Rect* rcSrc, SDL_Rect* rcDest) {

  SDL_Surface* oImage = IMG_Load(sImagePath);

  if (NULL == oImage) {
//    std::stringstream oStringStream = "Grafik " << sImagePath << " wurde nicht gefunden!";
//    throw std::runtime_error(oStringStream);
    throw std::runtime_error("Grafik konnte nicht geladen werden!");
  }

  SDL_Texture* oTexture = SDL_CreateTextureFromSurface(this->_oRenderer, oImage);

  scInstanz* oNewInstanz = new scInstanz();
  oNewInstanz->oTexture = oTexture;
  oNewInstanz->oImage = oImage;
  oNewInstanz->rcSrc = rcSrc;
  oNewInstanz->rcDest = rcDest;

  this->_oInstanzCollection.push_back(oNewInstanz);

  this->update();
}

bool cRenderer::initImage(const char* sImagePath) {

  SDL_Surface* oImage = IMG_Load(sImagePath);

  if (NULL == oImage) {
//    std::stringstream oStringStream = "Grafik " << sImagePath << " wurde nicht gefunden!";
//    throw std::runtime_error(oStringStream);

    throw std::runtime_error("Grafik konnte nicht geladen werden!");
  }

  SDL_Texture* oTexture = SDL_CreateTextureFromSurface(this->_oRenderer, oImage);

  scInstanz* oNewInstanz = new scInstanz();
  oNewInstanz->oTexture = oTexture;
  oNewInstanz->oImage = oImage;

  this->_oInstanzCollection.push_back(oNewInstanz);

  this->update();
}

cRenderer::cRenderer(SDL_Window* oWindow) {
  IMG_Init(IMG_INIT_JPG|IMG_INIT_PNG);

  this->_oWindow = oWindow;
  this->_oRenderer = SDL_CreateRenderer(this->_oWindow, -1, 0);
}

cRenderer::~cRenderer() {

  std::vector<scInstanz*>::iterator iter = this->_oInstanzCollection.begin();
  std::vector<scInstanz*>::iterator end = this->_oInstanzCollection.end();

  SDL_RenderClear(this->_oRenderer);
  while(iter != end)
  {
    SDL_DestroyTexture((*iter)->oTexture);
    SDL_FreeSurface((*iter)->oImage);
      ++iter;
  }

  SDL_DestroyRenderer(this->_oRenderer);

  IMG_Quit();
}

