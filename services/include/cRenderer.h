/*
 * cRenderer.h
 *
 *  Created on: 06.08.2016
 *      Author: mastercad
 */

#ifndef __SERVICES_CRENDERER_H__
#define __SERVICES_CRENDERER_H__

#include <vector>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

struct scInstanz {
    SDL_Texture* oTexture = NULL;
    SDL_Surface* oImage = NULL;
    SDL_Rect* rcSrc = NULL;
    SDL_Rect* rcDest = NULL;
};

class cRenderer {
  public:

    bool initImage(const char* sImagePath);
    bool initImage(const char* sImagePath, SDL_Rect* rcSrc, SDL_Rect* rcDest);
    bool update();

    cRenderer(SDL_Window* oWindow);
    virtual ~cRenderer();

  private:
    SDL_Window* _oWindow;
    SDL_Renderer* _oRenderer;
    std::vector<scInstanz*> _oInstanzCollection;
};

#endif /* SERVICES_CRENDERER_H_ */
