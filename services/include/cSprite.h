/*
 * cSprite.h
 *
 *  Created on: 06.08.2016
 *      Author: mastercad
 */

#ifndef __SERVICES_INCLUDE_CSPRITE_H__
#define __SERVICES_INCLUDE_CSPRITE_H__

#include <SDL2/SDL.h>

class cSprite {
  public:
    cSprite();
    virtual ~cSprite();

  private:
    SDL_Renderer* _oRenderer;
};

#endif /* SERVICES_INCLUDE_CSPRITE_H_ */
