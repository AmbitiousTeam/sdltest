/*
 * iObservable.h
 *
 *  Created on: 06.08.2016
 *      Author: mastercad
 */

#ifndef INTERFACES_IOBSERVABLE_H_
#define INTERFACES_IOBSERVABLE_H_

class iObservable {
  public:
    iObservable();
    virtual ~iObservable();
};

#endif /* INTERFACES_IOBSERVABLE_H_ */
