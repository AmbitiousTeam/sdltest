#ifndef __APP_INTERFACES_IMOVEABLE_H__
#define __APP_INTERFACES_IMOVEABLE_H__

#include <SDL2/SDL_image.h>

/**
 * Interface Moveable dient dem Notifier um bestimmte aktionen weiter zu leiten
 */
class iMoveable {
  public:
    bool notify(SDL_Event oEvent);
  
    iMoveable();
    ~iMoveable();
  private:
    SDL_Rect _rcSrc, _rcDest;
    Uint32 _iStepLength;
    bool _considerEvent(void);
    bool _considerScreenSize(void);
    SDL_Event _oEvent;
};

#endif
