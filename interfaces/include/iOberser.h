/*
 * iOberser.h
 *
 *  Created on: 06.08.2016
 *      Author: mastercad
 */

#ifndef INTERFACES_IOBERSER_H_
#define INTERFACES_IOBERSER_H_

class iOberser {
  public:
    iOberser();
    virtual ~iOberser();
};

#endif /* INTERFACES_IOBERSER_H_ */
