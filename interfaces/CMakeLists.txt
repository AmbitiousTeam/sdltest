file(GLOB INTERFACE_SOURCES "src/*.cpp")

if (INTERFACE_SOURCES)
    add_library (INTERFACES ${INTERFACE_SOURCES})
    
    target_include_directories (INTERFACES PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include)
endif()