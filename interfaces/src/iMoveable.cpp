#include "../include/iMoveable.h"

#define SCREEN_WIDTH  640
#define SCREEN_HEIGHT 480
#define SPRITE_SIZE   32

bool iMoveable::notify(SDL_Event oEvent) {
  bool bReturn = false;
  this->_oEvent = oEvent;

  if (true == this->_considerEvent()) {
	  bReturn = this->_considerScreenSize();
  }
  return (bReturn);
}

bool iMoveable::_considerEvent(void) {

  bool bReturn = false;
  
  switch (this->_oEvent.type) {
    
    /* close button clicked */
//    case SDL_QUIT:
//      bQuit = 1;
//      break;
    case SDL_KEYDOWN:
      switch (this->_oEvent.key.keysym.sym) {
        case SDLK_RSHIFT:
        case SDLK_LSHIFT:
          this->_iStepLength = 10;
          break;
        case SDLK_ESCAPE:
//		case SDLK_q:
//          bQuit = true;
//          break;
        case SDLK_LEFT:
          this->_rcDest.x -= this->_iStepLength;
          this->_rcSrc.y = SPRITE_SIZE;
          bReturn = true;
          break;
        case SDLK_RIGHT:
          this->_rcDest.x += this->_iStepLength;
          this->_rcSrc.y = 2 * SPRITE_SIZE;
          bReturn = true;
          break;
        case SDLK_UP:
          this->_rcDest.y -= this->_iStepLength;
          this->_rcSrc.y = 3 * SPRITE_SIZE;
          bReturn = true;
          break;
        case SDLK_DOWN:
          this->_rcDest.y += this->_iStepLength;
          this->_rcSrc.y = 0;
          bReturn = true;
          break;
      }
      break;
    case SDL_KEYUP:
      switch (this->_oEvent.key.keysym.sym) {
        case SDLK_RSHIFT:
        case SDLK_LSHIFT:
          this->_iStepLength = 5;
          break;
      }
  }
  return (bReturn);
}

bool iMoveable::_considerScreenSize() {
  
  bool bReturn = true;
  
  if (SCREEN_WIDTH - SPRITE_SIZE < this->_rcDest.x) {
    this->_rcDest.x = SCREEN_WIDTH - SPRITE_SIZE;
    bReturn = false;
  } else if (0 > this->_rcDest.x) {
    this->_rcDest.x = 0;
    bReturn = false;
  }
  
  if (SCREEN_HEIGHT - SPRITE_SIZE < this->_rcDest.y) {
    this->_rcDest.y = SCREEN_HEIGHT - SPRITE_SIZE;
    bReturn = false;
  } else if (0 > this->_rcDest.y) {
    this->_rcDest.y = 0;
    bReturn = false;
  }
  return (bReturn);
}

iMoveable::iMoveable() : _iStepLength(5) {

}

iMoveable::~iMoveable() {

}
