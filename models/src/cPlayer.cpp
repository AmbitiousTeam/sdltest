#include <iostream>
#include <stdexcept> // für exceptions
#include "../include/cPlayer.h"
#include "../../interfaces/include/iMoveable.h"

#define SCREEN_WIDTH  640
#define SCREEN_HEIGHT 480
#define SPRITE_SIZE   32

bool cPlayer::notify(SDL_Event oEvent) {
//  return (iMoveable::notify(oEvent));
  if (false == this->_bEventInProgress) {
    if (false == this->_handleEvent(oEvent)) {
      this->_rcSrc.x = SPRITE_SIZE;
    } else {
      this->_rcSrc.x = this->_iSpritePos * SPRITE_SIZE;
      this->_considerScreenSize();
    }
  }
  return (true);
}

bool cPlayer::update() {
  bool bReturn = true;

  this->_iTicks = SDL_GetTicks();
  this->_iSpritePos = (this->_iTicks / 100) % 3;

  this->_oRenderer->update();

  return (bReturn);
}

bool cPlayer::_considerScreenSize(void) {
  bool bReturn = true;

  if (SCREEN_WIDTH - SPRITE_SIZE < this->_rcDest.x) {
    this->_rcDest.x = SCREEN_WIDTH - SPRITE_SIZE;
    bReturn = false;
  } else if (0 > this->_rcDest.x) {
    this->_rcDest.x = 0;
    bReturn = false;
  }

  if (SCREEN_HEIGHT - SPRITE_SIZE < this->_rcDest.y) {
    this->_rcDest.y = SCREEN_HEIGHT - SPRITE_SIZE;
    bReturn = false;
  } else if (0 > this->_rcDest.y) {
    this->_rcDest.y = 0;
    bReturn = false;
  }
  return (bReturn);
}

int cPlayer::_convertMapPositionToArrayPosition(int iCoordinate) {
  return (roundf((float)iCoordinate / (float)SPRITE_SIZE));
}

bool cPlayer::_handleEvent(SDL_Event oEvent) {

  this->_bEventInProgress = true;

  bool bReturn = false;
  int iNewX = this->_rcDest.x;
  int iBoxX = iNewX;
  int iNewY = this->_rcDest.y;
  int iBoxY = iNewY;

  switch (oEvent.type) {
      /* handle the keyboard */
      case SDL_KEYDOWN:
        switch (oEvent.key.keysym.sym) {
          case SDLK_RSHIFT:
          case SDLK_LSHIFT:
            this->_iStepLength = 12;
            break;
          case SDLK_ESCAPE:
          case SDLK_LEFT:
            iNewX -= this->_iStepLength;
            iBoxX = iNewX;
            this->_rcSrc.y = SPRITE_SIZE;
            bReturn = true;
            break;
          case SDLK_RIGHT:
            iNewX += this->_iStepLength;
            iBoxX = iNewX + SPRITE_SIZE;
            this->_rcSrc.y = 2 * SPRITE_SIZE;
            bReturn = true;
            break;
          case SDLK_UP:
            iNewY -= this->_iStepLength;
            iBoxY = iNewY;
            this->_rcSrc.y = 3 * SPRITE_SIZE;
            bReturn = true;
            break;
          case SDLK_DOWN:
            iNewY += this->_iStepLength;
            iBoxY = iNewY + SPRITE_SIZE;
            this->_rcSrc.y = 0;
            bReturn = true;
            break;
        }
        break;
      case SDL_KEYUP:
        switch (oEvent.key.keysym.sym) {
          case SDLK_RSHIFT:
          case SDLK_LSHIFT:
            this->_iStepLength = 6;
            break;
        }
  }

  int iArrayColumn = this->_convertMapPositionToArrayPosition(iBoxX);
  int iArrayRow = this->_convertMapPositionToArrayPosition(iBoxY);

  std::cout << "Array Spalte : " << iArrayColumn << " - Array Zeile : " << iArrayRow << std::endl;
  std::cout << this->_rcDest.x << " x " << this->_rcDest.y << std::endl;
  std::cout << iBoxX << " x " << iBoxY << std::endl;
  std::cout << iNewX << " x " << iNewY << std::endl;

  if (true == this->_checkValidPosition(iBoxX, iBoxY)) {
    std::cout << "Position ist ok!" << std::endl;
    this->_adjustNewPlayerPosition(iNewX, iNewY);
  }

  std::cout << this->_rcDest.x << " x " << this->_rcDest.y << std::endl;
  std::cout << "#####################################################################" << std::endl;

  this->_bEventInProgress = false;

  return (bReturn);
}

void cPlayer::_adjustNewPlayerPosition(int iPositionX, int iPositionY) {
//  this->_rcDest.x = SPRITE_SIZE * round(iPositionX / SPRITE_SIZE);
//  this->_rcDest.y = SPRITE_SIZE * round(iPositionY / SPRITE_SIZE);
  this->_rcDest.x = iPositionX;
  this->_rcDest.y = iPositionY;
}

bool cPlayer::_checkValidPosition(int iCurrentColumn, int iCurrentRow) {
  bool bReturn = false;

  int iArrayColumn = this->_convertMapPositionToArrayPosition(iCurrentColumn);
  int iArrayRow = this->_convertMapPositionToArrayPosition(iCurrentRow);

  std::cout << "CurrentColumn : " << iCurrentColumn << " CurrentRow : " << iCurrentRow <<
      " Column : " << iArrayColumn << " Row : " << iArrayRow <<
      " Sizeof Columns : " << (sizeof(this->_aiMap) / sizeof(*this->_aiMap)) <<
      " Content Current Position : " << this->_aiMap[iArrayRow][iArrayColumn] << std::endl;

  if (0 == this->_aiMap[iArrayRow][iArrayColumn]) {
    bReturn = true;
  }
  return (bReturn);
}

void cPlayer::setMap(int** aiMap) {

  int iRows = 15;
  int iCols = 20;

  for (int i = 0; i < iRows; ++i) {
    std::cout << "Row : " << i << " : ";
    for (int j = 0; j < iCols; ++j) {
      std::cout << aiMap[i][j] << " | ";
    }
    std::cout << std::endl;
  }
  this->_aiMap = aiMap;
}

int** cPlayer::getMap() {
  return (this->_aiMap);
}

cPlayer::cPlayer(cRenderer* oRenderer) :
    _aiMap(NULL),
    _bEventInProgress(false),
    _iCurrentMapColumn(SPRITE_SIZE),
    _iCurrentMapRow(0),
    _iDirection(0),
    _iTicks(0),
    _iSpritePos(0),
    _iStepLength(6),
    _oRenderer(NULL)
{
  this->_oRenderer = oRenderer;

  this->_rcDest = { this->_iCurrentMapColumn, this->_iCurrentMapRow, SPRITE_SIZE, SPRITE_SIZE};
  this->_rcSrc = { this->_iSpritePos * SPRITE_SIZE, this->_iDirection, SPRITE_SIZE, SPRITE_SIZE };

  this->_oRenderer->initImage("Images/prussia_sprite_sheet_by_animatefox-d2zyin9.png", &this->_rcSrc, &this->_rcDest);
}

cPlayer::~cPlayer() {

}
