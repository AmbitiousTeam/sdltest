#ifndef __CPLAYER_H__
#define __CPLAYER_H__
#include <SDL2/SDL_video.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_events.h>

#include "cPerson.h"
#include "../../services/include/cRenderer.h"

/**
 * spieler klasse
 */
class cPlayer : public cPerson {
  public:
    
    bool notify(SDL_Event oEvent);
    bool update(void);
    
    void setMap(int** aiMap);
    int** getMap();

    cPlayer(cRenderer* oRenderer);
    ~cPlayer();
  protected:
    
  private:
    
    bool _considerScreenSize(void);
    bool _handleEvent(SDL_Event oEvent);
    int _convertMapPositionToArrayPosition(int);
    bool _checkValidPosition(int, int);
    void _adjustNewPlayerPosition(int, int);

    int** _aiMap;
    bool _bEventInProgress;
    Uint32 _iCurrentMapColumn;
    Uint32 _iCurrentMapRow;
    Uint32 _iDirection;
    Uint32 _iTicks;
    Uint32 _iSpritePos;
    Uint32 _iStepLength;
    SDL_Rect _rcSrc;
    SDL_Rect _rcDest;
    cRenderer* _oRenderer;
};

#endif
