#ifndef __MODELS_CPERSON_H__
#define __MODELS_CPERSON_H__

#include "../../interfaces/include/iMoveable.h"

class cPerson : public iMoveable {
  public:
    cPerson();
    ~cPerson();
};

#endif
