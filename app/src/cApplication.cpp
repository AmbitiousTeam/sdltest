/*
 * cApplication.cpp
 *
 *  Created on: 06.08.2016
 *      Author: mastercad
 */
#include <iostream>
#include <stddef.h> // includiert für NULL
#include <stdexcept> // für exceptions

#include "../include/cApplication.h"

#define SCREEN_WIDTH  640
#define SCREEN_HEIGHT 480

void cApplication::run(void) {
  this->_generateMap();
  this->_oPlayer->setMap(this->_aiMap);
  this->_considerEvents();
}

void cApplication::_considerEvents(void) {

  while(false == this->_bQuit) {
    // Player aktionen durchführen, die unabhängig von events sind
    if (NULL != this->_oPlayer) {
      this->_oPlayer->update();
    }
    if (SDL_PollEvent(&this->_oEvent)) {
      // events berücksichtigen
      this->_handleCurrentEvent();
      this->_oPlayer->notify(this->_oEvent);
    }
  }
}

void cApplication::_generateMap() {

  int iRows = 15;
  int iCols = 20;
  this->_aiMap = new int*[iRows];

  this->_aiMap[0] =   new  int[iCols]{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1};
  this->_aiMap[1] =   new  int[iCols]{1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1};
  this->_aiMap[2] =   new  int[iCols]{1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1};
  this->_aiMap[3] =   new  int[iCols]{1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 1};
  this->_aiMap[4] =   new  int[iCols]{1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1};
  this->_aiMap[5] =   new  int[iCols]{1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1};
  this->_aiMap[6] =   new  int[iCols]{1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1};
  this->_aiMap[7] =   new  int[iCols]{1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1};
  this->_aiMap[8] =   new  int[iCols]{1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
  this->_aiMap[9] =   new  int[iCols]{1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1};
  this->_aiMap[10] =  new  int[iCols]{1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1};
  this->_aiMap[11] =  new  int[iCols]{1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1};
  this->_aiMap[12] =  new  int[iCols]{1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1};
  this->_aiMap[13] =  new  int[iCols]{1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1};
  this->_aiMap[14] =  new  int[iCols]{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1};

//  for (int iRow = 0; iRow < iRows; ++iRow) {
//    this->_aiMap[iRow] = new int[iCols]{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1};
////    for (int iCol = 0; iCol < iCols; ++iCol) {
////      this->_aiMap[iRow][iCol] = iCol;
////    }
//  }
/*
  for (int i = 0; i < iRows; ++i) {
    std::cout << "Row : " << i << " : ";
    for (int j = 0; j < iCols; ++j) {
      std::cout << this->_aiMap[i][j] << " | ";
    }
    std::cout << std::endl;
  }
*/
}

bool cApplication::createPlayer(std::string sName) {

  this->_oPlayer = new cPlayer(this->_oRenderer);
}

void cApplication::_handleCurrentEvent(void) {

  switch (this->_oEvent.type) {
    /* close button clicked */
    case SDL_QUIT:
      this->_bQuit = true;
      break;
      /* handle the keyboard */
      case SDL_KEYDOWN:
        switch (this->_oEvent.key.keysym.sym) {
          case SDLK_ESCAPE:
          case SDLK_q:
            this->_bQuit = true;
            break;
        }
  }
}

cApplication::cApplication() :
    _oWindow(NULL),
    _aiMap(NULL),
    _oRenderer(NULL),
    _oPlayer(NULL),
    _bQuit(false)
{
  if (0 < SDL_Init(SDL_INIT_VIDEO)) {
    throw std::runtime_error("Es ist ein Fehler beim initialisieren der SDL aufgetreten!");
  }

  this->_oWindow = SDL_CreateWindow(
      "TITEL!", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, 0);

  if (NULL == this->_oWindow) {
    throw std::runtime_error("Es ist ein Fehler beim erstellen des Fensters aufgetreten!");
  }

  this->_oRenderer = new cRenderer(this->_oWindow);

//  this->_oRenderer->initImage("Images/sdl_image-pict3159.jpg");
  this->_oRenderer->initImage("Images/map_bw.png");
//  this->_oRenderer->initImage("Images/6043983.png");
}

cApplication::~cApplication() {
  SDL_DestroyWindow(this->_oWindow);
  SDL_Quit();
}

