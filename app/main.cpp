#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "include/cApplication.h"

bool bQuit = false;
SDL_Rect rcSrc, rcDest;
Uint32 iDirection = 0;
Uint32 iStepLength = 5;

#define SCREEN_WIDTH  640
#define SCREEN_HEIGHT 480
#define SPRITE_SIZE   32

bool handleEvent(SDL_Event oEvent) {
  bool bReturn = false;
  switch (oEvent.type) {
    /* close button clicked */
    case SDL_QUIT:
      bQuit = 1;
      break;
      
      /* handle the keyboard */
      case SDL_KEYDOWN:
        switch (oEvent.key.keysym.sym) {
          case SDLK_RSHIFT:
          case SDLK_LSHIFT:
            iStepLength = 10;
            break;
          case SDLK_ESCAPE:
          case SDLK_q:
            bQuit = true;
            break;
          case SDLK_LEFT:
            rcDest.x -= iStepLength;
            rcSrc.y = SPRITE_SIZE;
            bReturn = true;
            break;
          case SDLK_RIGHT:
            rcDest.x += iStepLength;
            rcSrc.y = 2 * SPRITE_SIZE;
            bReturn = true;
            break;
          case SDLK_UP:
            rcDest.y -= iStepLength;
            rcSrc.y = 3 * SPRITE_SIZE;
            bReturn = true;
            break;
          case SDLK_DOWN:
            rcDest.y += iStepLength;
            rcSrc.y = 0;
            bReturn = true;
            break;
        }
        break;
      case SDL_KEYUP:
        switch (oEvent.key.keysym.sym) {
          case SDLK_RSHIFT:
          case SDLK_LSHIFT:
            iStepLength = 5;
            break;
        }
  }
  return (bReturn);
}

bool considerScreenSize() {
  bool bReturn = true;
  if (SCREEN_WIDTH - SPRITE_SIZE < rcDest.x) {
    rcDest.x = SCREEN_WIDTH - SPRITE_SIZE;
    bReturn = false;
  } else if (0 > rcDest.x) {
    rcDest.x = 0;
    bReturn = false;
  }
  
  if (SCREEN_HEIGHT - SPRITE_SIZE < rcDest.y) {
    rcDest.y = SCREEN_HEIGHT - SPRITE_SIZE;
    bReturn = false;
  } else if (0 > rcDest.y) {
    rcDest.y = 0;
    bReturn = false;
  }
  return (bReturn);
}

int main(int argc, char **argv) {
  
  cApplication* oApplication = new cApplication();
  oApplication->createPlayer("Erster");
  oApplication->run();
  
  return (0);
}
