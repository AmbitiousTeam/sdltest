/*
 * cApplication.h
 *
 *  Created on: 06.08.2016
 *      Author: mastercad
 */

#ifndef __APP_CAPPLICATION_H_
#define __APP_CAPPLICATION_H_

#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "../../models/include/cPlayer.h"
#include "../../services/include/cRenderer.h"

class cApplication {
  public:
    void run(void);
    bool createPlayer(std::string sName);

    cApplication();
    virtual ~cApplication();

    bool isQuit() const {
      return (_bQuit);
    }

    void setQuit(bool quit) {
      _bQuit = quit;
    }

  private:

    void _generateMap(void);
    void _considerEvents(void);
    void _handleCurrentEvent(void);

    SDL_Window* _oWindow;
    SDL_Event _oEvent;

    int** _aiMap;
    cRenderer* _oRenderer;
    cPlayer* _oPlayer;

    bool _bQuit;
};

#endif /* APP_CAPPLICATION_H_ */
